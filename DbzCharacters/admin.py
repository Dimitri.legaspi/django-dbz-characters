from django.contrib import admin
from DbzCharacters.models import Character, SpecificCharacter

# Register your models here.
@admin.register(Character)
class CharacterAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'id',
    )
    
@admin.register(SpecificCharacter)
class SpecfificCharacterAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'id',
    )