from django.urls import path
from DbzCharacters.views import character_list, show_character, add_character, add_specifics, edit_character, edit_specific

urlpatterns = [
    path("", character_list, name="character_list"),
    path("<int:id>", show_character, name="show_character"),
    path("add character/", add_character, name="add_character"),
    path("add character2/", add_specifics, name="add_specifics"),
    path("<int:id>/edit/", edit_character, name="edit_character"),
    path("<int:id>/edit2/", edit_specific, name="edit_specific"),
]
