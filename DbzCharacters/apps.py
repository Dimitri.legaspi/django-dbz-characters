from django.apps import AppConfig


class DbzcharactersConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "DbzCharacters"
