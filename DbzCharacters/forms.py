from django.forms import ModelForm
from DbzCharacters.models import Character, SpecificCharacter

class CharacterForm(ModelForm):
    class Meta:
        model = Character
        fields = ["name", "picture",]
        
class SpecificCharacterForm(ModelForm):
    class Meta:
        model = SpecificCharacter
        fields = ["name", "image", "bio"]