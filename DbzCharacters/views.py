from django.shortcuts import render, redirect, get_object_or_404
from DbzCharacters.models import Character, SpecificCharacter
from DbzCharacters.forms import CharacterForm, SpecificCharacterForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def character_list(request):
    characters = Character.objects.all()
    context = {
        "characters":characters
    }
    return render(request, "dbz/list.html", context)

def show_character(request, id):
    character = SpecificCharacter.objects.get(id=id)
    context = {
        "character":character
    }
    return render(request, "dbz/detail.html", context)

@login_required
def add_character(request):
    if request.method == "POST":
        form = CharacterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("add_specifics")
    else:
        form = CharacterForm()
    context = {
        "form":form
    }
    return render(request, "dbz/add.html", context)


@login_required
def add_specifics(request):
    if request.method == "POST":
        form = SpecificCharacterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("character_list")
    else:
        form = SpecificCharacterForm()
    context = {
        "form":form
    }
    return render(request, "dbz/add2.html", context)


@login_required
def edit_character(request, id):
    post = get_object_or_404(Character, id=id)
    if request.method == "POST":
        form = CharacterForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            
            return redirect("edit_specific", id=id)
    else:
        form = CharacterForm(instance=post)
    context = {
        "post":post,
        "form":form,
    }
    return render(request, "dbz/edit.html", context)


@login_required
def edit_specific(request, id):
    post = get_object_or_404(SpecificCharacter, id=id)
    if request.method == "POST":
        form = SpecificCharacterForm(request.POST, instance=post)
        if form.is_valid():
            form.save()
            
            return redirect("show_character", id=id)
    else:
        form = SpecificCharacterForm(instance=post)
    context = {
        "post":post,
        "form":form,
    }
    return render(request, "dbz/edit2.html", context)