from django.db import models

# Create your models here.
class Character(models.Model):
    name = models.CharField(max_length=150)
    picture = models.URLField()
    
    def __str__(self):
        return self.name
    
class SpecificCharacter(models.Model):
    name = models.ForeignKey(
        Character,
        related_name="names",
        on_delete=models.CASCADE,
    )
    image = models.URLField()
    bio = models.TextField()
    